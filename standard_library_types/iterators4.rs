// iterators4.rs

struct Factorial {
    index: u64,
    product: u64,
}

impl Default for Factorial {
    fn default() -> Self {
        Self {
            index: 0,
            product: 1,
        }
    }
}

impl Factorial {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Iterator for Factorial {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;
        self.product *= self.index;
        Some(self.product)
    }
}

pub fn factorial(num: u64) -> u64 {
    // Complete this function to return the factorial of num
    // Do not use:
    // - return
    // Try not to use:
    // - imperative style loops (for, while)
    // - additional variables
    // For an extra challenge, don't use:
    // - recursion
    // Execute `rustlings hint iterators4` for hints.
    use std::convert::TryInto;
    Factorial::default()
        .nth((num - 1).try_into().unwrap())
        .unwrap()
    // https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.product
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn factorial_of_1() {
        assert_eq!(1, factorial(1));
    }
    #[test]
    fn factorial_of_2() {
        assert_eq!(2, factorial(2));
    }

    #[test]
    fn factorial_of_4() {
        assert_eq!(24, factorial(4));
    }
}
